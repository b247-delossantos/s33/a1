

fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(json => {
    let titles = json.map(item => {return item.title});
    console.log(titles);
  })

 
fetch('https://jsonplaceholder.typicode.com/todos/1')
  .then(response => response.json())
  .then(json => console.log(json))

  fetch('https://jsonplaceholder.typicode.com/todos/1')
  .then(response => response.json())
  .then(json => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`))




fetch('https://jsonplaceholder.typicode.com/todos', {
  method: 'POST',
  body: JSON.stringify({
    title: 'Create to do List Item',
    completed: false,
    userId: 1
  }),
  headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  })
  .then(response => response.json())
  .then(json => console.log(json))



fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PUT',
  body: JSON.stringify({
    id: 1,
    title: 'Updated to do List Item',
    dateCompleted: "Pending",
    description: "To update the my to do list with a different data structure",
    status: "Pending",
    userId: 1

  }),
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }
  })
  .then(response => response.json())
  .then(json => console.log(json))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PATCH',
  body: JSON.stringify({
    id: 1,
    title: "delectus aut autem",
    dateCompleted: "07/09/21",
    completed: false,
    status: "Complete",
    userId: 1

  }),
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }
  })
  .then(response => response.json())
  .then(json => console.log(json))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE',
  body: JSON.stringify({
    userId: 1
  }),
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }
})
.then(response => response.json())
.then(json => console.log(json))
